package org.bitbucket.sergey_ivanenko.task7.di

import dagger.Component
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.bitbucket.sergey_ivanenko.task7.data.remote.api.ApiService
import org.bitbucket.sergey_ivanenko.task7.data.remote.repository.DataRepositoryImpl
import org.bitbucket.sergey_ivanenko.task7.domain.DataRepository
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Component(modules = [AppModule::class])
@Singleton
interface AppComponent {
    fun mainViewModelFactory(): ViewModelFactory
}

@Module(includes = [NetworkModule::class])
object AppModule {

    @Provides
    @Singleton
    fun provideDataRepository(apiService: ApiService): DataRepository =
        DataRepositoryImpl(apiService)
}

@Module
object NetworkModule {

    @Provides
    @Singleton
    fun provideHttpLoginInterceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        return httpLoggingInterceptor
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(httpLoginInterceptor: HttpLoggingInterceptor) =
        OkHttpClient.Builder()
            .addInterceptor(httpLoginInterceptor)
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): ApiService =
        retrofit.create(ApiService::class.java)
}

private const val BASE_URL = "http://test.clevertec.ru/tt/"