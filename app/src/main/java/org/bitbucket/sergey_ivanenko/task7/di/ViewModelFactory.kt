package org.bitbucket.sergey_ivanenko.task7.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.bitbucket.sergey_ivanenko.task7.domain.DataRepository
import org.bitbucket.sergey_ivanenko.task7.presentation.viewmodel.MainViewModel
import javax.inject.Inject

class ViewModelFactory @Inject constructor(
    private val repository: DataRepository,
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(repository) as T
    }
}
