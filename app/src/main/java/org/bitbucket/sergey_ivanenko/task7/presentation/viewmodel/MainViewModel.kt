package org.bitbucket.sergey_ivanenko.task7.presentation.viewmodel;

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.bitbucket.sergey_ivanenko.task7.data.remote.api.DataResponse
import org.bitbucket.sergey_ivanenko.task7.domain.DataRepository
import javax.inject.Inject

class MainViewModel @Inject constructor(private val repository: DataRepository) : ViewModel() {

    private val _data = MutableLiveData<DataResponse>()
    val metadata: LiveData<DataResponse> get() = _data

    fun getData() {
        viewModelScope.launch {
            repository.fetchMetaData().collect {
                _data.postValue(it)
            }
        }
    }
}
