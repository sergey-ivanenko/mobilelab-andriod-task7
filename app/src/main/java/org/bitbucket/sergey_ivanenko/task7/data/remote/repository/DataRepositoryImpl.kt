package org.bitbucket.sergey_ivanenko.task7.data.remote.repository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import org.bitbucket.sergey_ivanenko.task7.data.remote.api.ApiService
import org.bitbucket.sergey_ivanenko.task7.data.remote.api.DataResponse
import org.bitbucket.sergey_ivanenko.task7.domain.DataRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepositoryImpl @Inject constructor(private val apiService: ApiService) : DataRepository {

    override suspend fun fetchMetaData(): Flow<DataResponse> = flow {
        emit(apiService.fetchMetaData())
    }.flowOn(Dispatchers.IO)
}