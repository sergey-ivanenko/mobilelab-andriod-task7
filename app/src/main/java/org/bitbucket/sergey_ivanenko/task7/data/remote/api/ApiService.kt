package org.bitbucket.sergey_ivanenko.task7.data.remote.api

import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiService {

    @GET(GET_DATA)
    @Headers("Content-Type: application/json")
    suspend fun fetchMetaData(): DataResponse

    companion object {
        private const val GET_DATA = "meta"
    }
}