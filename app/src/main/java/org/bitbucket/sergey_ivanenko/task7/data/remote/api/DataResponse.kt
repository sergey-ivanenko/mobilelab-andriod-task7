package org.bitbucket.sergey_ivanenko.task7.data.remote.api

data class DataResponse(
    val fields: List<Field>,
    val image: String,
    val title: String
)