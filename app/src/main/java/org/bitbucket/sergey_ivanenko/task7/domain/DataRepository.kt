package org.bitbucket.sergey_ivanenko.task7.domain

import kotlinx.coroutines.flow.Flow
import org.bitbucket.sergey_ivanenko.task7.data.remote.api.DataResponse

interface DataRepository {
    suspend fun fetchMetaData(): Flow<DataResponse>
}