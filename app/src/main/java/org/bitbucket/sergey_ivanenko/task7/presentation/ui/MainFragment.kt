package org.bitbucket.sergey_ivanenko.task7.presentation.ui

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import org.bitbucket.sergey_ivanenko.task7.R
import org.bitbucket.sergey_ivanenko.task7.appComponent
import org.bitbucket.sergey_ivanenko.task7.databinding.FragmentMainBinding
import org.bitbucket.sergey_ivanenko.task7.presentation.viewmodel.MainViewModel

class MainFragment : Fragment(R.layout.fragment_main) {

    private val binding: FragmentMainBinding by lazy {
        FragmentMainBinding.inflate(layoutInflater)
    }

    private val mainViewModel: MainViewModel by viewModels { appComponent.mainViewModelFactory() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        mainViewModel.getData()
        Log.d("TAG", "FRAGMENT")
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mainViewModel.metadata.observe(viewLifecycleOwner) {
            binding.apply {
                toolbar.title = it.title
                textView.text = it.toString()
                Log.d("TAG", it.toString())
                Glide.with(this@MainFragment)
                    .load(it.image)
                    .error(R.drawable.ic_error_48)
                    .into(image)
            }
        }
    }
}