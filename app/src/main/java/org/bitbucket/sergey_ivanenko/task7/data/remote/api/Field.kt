package org.bitbucket.sergey_ivanenko.task7.data.remote.api

data class Field(
    val name: String,
    val title: String,
    val type: String,
    val values: Values?
)