package org.bitbucket.sergey_ivanenko.task7

import android.app.Application
import android.content.Context
import androidx.fragment.app.Fragment
import org.bitbucket.sergey_ivanenko.task7.di.AppComponent
import org.bitbucket.sergey_ivanenko.task7.di.DaggerAppComponent

class App : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.create()
    }
}

val Context.appComponent: AppComponent
    get() = when (this) {
        is App -> appComponent
        else -> applicationContext.appComponent
    }

val Fragment.appComponent: AppComponent
    get() = requireContext().appComponent