package org.bitbucket.sergey_ivanenko.task7.presentation.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import org.bitbucket.sergey_ivanenko.task7.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, MainFragment())
                .commit()
        }
    }
}