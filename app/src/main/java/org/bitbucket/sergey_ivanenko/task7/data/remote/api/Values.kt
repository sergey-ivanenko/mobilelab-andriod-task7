package org.bitbucket.sergey_ivanenko.task7.data.remote.api

data class Values(
    val none: String,
    val v1: String,
    val v2: String,
    val v3: String
)